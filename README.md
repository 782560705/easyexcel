#easyExcel

## 关于

该项目实现了列表数据的导入，分页列表导出，支持自定义格式，支持模版以及模板和列表的混合导出，小巧、简易、高性能。


##版本号：0.2


##更新说明


注解类：

	Excel注解类，此注解类用于类上面，定义了读写excel所需要的属性，属性：
	
		beginRow：读写的开始行，不写默认第一行开始
		
		isNeedSequence：生成时是否生成序号列，默认生成
		
		dataHeader：生成时列头名称以及对就的列以及列的宽度，格式为:  字段名:哪一列:列宽,格式为,字段名:哪一列:列宽...
					可以写成：  字段名,字段名...或字段名:哪一列,格式为,字段名:哪一列...  注意省略值为默认
		
		outFilePath：生成路径
		
		inFilePath：读取路径
		
		autoHeight:是否自适应高度,默认不自适应
		
		createRowWay：创建行的方式，二个取值add,insert。add新增一行，insert插入一行，默认新增
	
	Sheet注解类，此类用于类上，定义相关sheet属性，属性：
	
		sheetName：生成时sheet的名称
		
		sheetNum：读写时确认哪一个sheet,默认第一个
		
		sheetSize：分面的数据量，在页多少条，默认不分页
		
	Cell注解类，此类用于字段上，确认字段和cell的匹配，属性：
	
		rowNum：哪一行
		
		columnNum：哪一列，支持大小写字母以及数字
		
		name:标签名称，当字段和标签名不一致时使用
		
		dateFormat:日期格式，默认格式为：yyyy/MM/dd
		
	Ingore注解类，此类用于字段上，标识字段是否需要读写操作
	
	ListExcel注解类，此注解用于对象和集合的混合导出，属性：
		beginRowName：启始行的标签名，当属性和标签名不一致时使用
		
	Select注解类：标识下拉框  注：只支持hssfworkbook和xssfworkbook
	
功能以及用法：

	1.读取列表数据：
	
		例子请看代码测试，路径： EASYEXCEL\test\com\easyexcel\readlisttest，注意文件路径
		
			代码片段：
			类：
			@Excel(beginRow=2,inFilePath="d:\\students.xlsx")
			public class Students {
				@Cell(columnNum="2")
				private String name;
				@Cell(columnNum="c")//or@Cell(whichCell="C")
				private int age;
			}
			使用：
			ReadExcel<Students> re = new ReadListExcel<>();
			//Map<String,Object> param = new HashMap<String, Object>();
			//List<Students> list = re.read(param,Students.class);
			List<Students> list = re.read(Students.class);
			
	2.读取模版数据	
	
		a.用标签读取模版数据，例子请看代码测试，路径： EASYEXCEL\test\com\easyexcel\readmodeloflabletest，注意文件路径
		
			代码片段：
			类：
			public class Students {
				private String name;
				private int age;
			}
			使用：
			ReadExcel<Students> re = new ReadModelExcel<>();
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("inFilePath","d:\\studentsModel.xlsx");
			List<Students> list = re.read(param,Students.class);
		
		b.用注解读取模版数据，例子请看代码测试，路径： EASYEXCEL\test\com\easyexcel\readmodelofannotationtest，注意文件路径
		
			代码片段：
			类：
			@Excel(inFilePath="d:\\studentsModelann.xlsx")
			public class Students {
				@Cell(rowNum = 5,columnNum="b")
				private String name;
				@Cell(rowNum = 2,columnNum="c")//or@Cell(columnNum="C")
				private int age;
			}
			使用：
			ReadExcel<Students> re = new ReadModelExcel<>();
			//List<Students> list = re.read(null,Students.class);
			List<Students> list = re.read(Students.class)
			
		注意事项目：	
			读取excel传参说明：
				inFilePath：读取路径,可以用注解，如果传，则以传入为主
				beginRow：开始行，可以用注解，如果传，以传入为主
		
	3.写入列表数据：
		
		a.一个是通用的实现，例子请看代码测试，码路径：EASYEXCEL\test\com\easyexcel\writelistTest 方法为：testCommon
			
			代码片段：
			类：
			@Excel(beginRow=1,dataHeader="姓名:1,年龄:3,成绩:4",outFilePath="d:\\test.xls")
			@Sheet(/*sheetSize=2000,*/sheetName="测试")
			public class Students {
				@Cell(columnNum="1")
				private String name;
				@Cell(columnNum="c")
				private int age;
				@Cell(columnNum="4")
				private double grade;
			}
			使用：
			WriteExcel we = new WriteListExcel();
			//Map<String,Object> param = new HashMap<String, Object>();
			List<Students> list = new ArrayList<Students>();
				for(int i=1;i<5000;i++){
				list.add(new Students("name"+i,i,i));
			}
			//we.write(param, list);
			we.write(list);
			
		b.另一个是根据自己需求再次调整行的样式，比如合并，例子请看代码测试，路径：EASYEXCEL\test\com\easyexcel\writelistTest 方法为：testCustom
			代码片段：
				类：
				@Excel(beginRow=1,dataHeader="姓名:1,年龄:3,成绩:4",outFilePath="d:\\test.xls")
				@Sheet(/*sheetSize=2000,*/sheetName="测试")
				public class Students {
					@Cell(columnNum="1")
					private String name;
					@Cell(columnNum="c")
					private int age;
					@Cell(columnNum="4")
					private double grade;
				}
				使用：
				Workbook workbook = new HSSFWorkbook();
				Sheet sheet = workbook.createSheet("test");
				int beginNum = 1;
				MyCellStyle cellStyle = new CommonCellStyle(workbook);
				WriteListExcelHelp weh = new WriteListExcelHelp(cellStyle);
				Map<String,Object> param = new HashMap<String, Object>();
				List<Students> list = new ArrayList<Students>();
				for(int i=1;i<5000;i++){
					list.add(new Students("name"+i,i,i));
				}
				weh.generateHeader(sheet, beginNum, Students.class);
				CellStyle commonCellStyle = cellStyle.createCommonCellStyle();
				for(Students s : list){
					Row row = weh.generateBody(sheet, ++beginNum, s);
					CellRangeAddress cra = new CellRangeAddress(beginNum-1, beginNum-1, 1, 2);
					sheet.addMergedRegion(cra);
					row.createCell(2).setCellStyle(commonCellStyle);
				}
				ExcelUtil.workbookToFile(workbook, "d:\\test1.xls");
			
		注意事项目：
		
			在写放时需要传下面几个参数：
			
				myCellStyle：样式类，不传则为默认实现，样式定义一个样式接口MyCellStyle,如需改动请自己实现。
				
				workbook：workbook实现类，生成excel的文件类型，默认为：hssfworkbook
				
				outFilePath:生成文件路径，可以用注解，如果传，则以传入为主
				
				inFilePath：读取路径,可以用注解，如果传，则以传入为主
				
				beginRow：开始行，可以用注解，如果传，以传入为主
				
	4.写入模版数据，即定义好模版，读取模版并在模版相应位置写入数据，二种实现方案：
		
		a.用标签来定位cell,即在excel模版相应cell上写入标签，例子请看代码测试，路径：EASYEXCEL\test\com\easyexcel\writemodeloflaabletest，注意文件路径
			
			代码片段：
			类：
			public class Students {
				private String name;
				@Cell(name = "age")
				private int nl;
				@ListExcel(beginRowName="grade")
				private List<Grade> grades = new ArrayList<Grade>();
			}
			
			@Excel(beginRow=5,dataHeader="课目:1,分数:2")
			public class Grade {
				@Cell(columnNum="1")
				private String topic;
				@Cell(columnNum="2")
				private double score;
			}
			使用：
			WriteExcel we = new WriteModelExcel();
			Map<String,Object> param = new HashMap<>();
			param.put("inFilePath", "d:\\model.xlsx");//读取文件的目录必须有，可以传也可以用注解配,如果传以传为主
			param.put("outFilePath", "d:\\outmodel.xlsx");//生成路径必须有，可以传也可以用注解配,如果传以传为主
			List<Students> list = new ArrayList<Students>();
			Students students = new Students("张三2222222222222222222222222222222",25);
			List<Grade> grades = new ArrayList<Grade>();
			Grade grade = new Grade("数学",99);
			grades.add(grade);
			students.setGrades(grades);
			list.add(students);
			we.write(param, list);
		b.用注解来定位cell,即在类字段上用注解表时位置，如果有注解，以注解实现，例子请看代码测试，路径：EASYEXCEL\test\com\easyexcel\writemodelofannotationtest，注意文件路径
			
			代码片段：
			类：
			@Excel(inFilePath="d:\\modelann.xlsx",outFilePath="d:\\outmodelann.xlsx")
			@Sheet(sheetNum=1)
			public class Students {
				@Cell(rowNum=1,columnNum="b")
				private String name;
				@Cell(rowNum=3,columnNum="b")
				private int age;
				@Ingroe
				private double  price;
				@ListExcel
				private List<Grade> grades = new ArrayList<Grade>();
			}
			@Excel(beginRow=5,dataHeader="课目:1,分数:2")
			public class Grade {
				@Cell(columnNum="1")
				private String topic;
				@Cell(columnNum="2")
				private double score;
			}
			使用：
			WriteExcel we = new WriteModelExcel();
			//Map<String,Object> param = new HashMap<>();
			List<Students> list = new ArrayList<Students>();
			Students students = new Students("张三2222222222222222222222222222222",25);
			List<Grade> grades = new ArrayList<Grade>();
			Grade grade = new Grade("数学",99);
			grades.add(grade);
			students.setGrades(grades);
			list.add(students);
			//we.write(param, list);
			we.write(list);
			
	
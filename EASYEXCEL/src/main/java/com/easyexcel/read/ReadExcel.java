package com.easyexcel.read;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.easyexcel.read.exception.LackCellAnnotation;

public interface ReadExcel<T> {

    List<T> read(Map<String, Object> param, Class clazz) throws IOException, InstantiationException,
            IllegalAccessException;

    List<T> read(Class clazz) throws IOException, InstantiationException,
            IllegalAccessException;
}

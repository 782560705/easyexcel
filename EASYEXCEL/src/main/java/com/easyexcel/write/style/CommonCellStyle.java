package com.easyexcel.write.style;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

public class CommonCellStyle extends MyCellStyle{
	public CommonCellStyle(Workbook workbook) {
		super(workbook);
	}
	//生成一般样式
	public CellStyle createCommonCellStyle() {
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle .setWrapText(true);
		Font font = workbook.createFont();
		font.setFontName("宋体");
		font.setFontHeightInPoints((short) 11);
		cellStyle.setFont(font);
		return cellStyle;
	}
	//生成一般数字样式
	public CellStyle createCommonNumberCellStyle() {
		CellStyle cellStyle = createCommonCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		return cellStyle;
	}
	//生成一般文字样式
	public CellStyle createCommonTextCellStyle() {
		CellStyle cellStyle = createCommonCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.LEFT);
		return cellStyle;
	}
	//生成一般列头样式
	public CellStyle createCommonHeaderCellStyler() {
		CellStyle cellStyle = createCommonCellStyle();
		Font font = workbook.createFont();
		font.setFontName("宋体");
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		cellStyle.setFont(font);
		return cellStyle;
	}
	
	
}	

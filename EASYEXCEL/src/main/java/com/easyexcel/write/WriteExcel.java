package com.easyexcel.write;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface WriteExcel {
    /**
     * 默认样式写入
     *
     * @param param
     * @param list
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    boolean write(Map<String, Object> param, List list) throws IOException, IllegalArgumentException, IllegalAccessException;

    /**
     * 默认样式写入
     *
     * @param list
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    boolean write(List list) throws IOException, IllegalArgumentException, IllegalAccessException;
}

package com.easyexcel.readModelOfAnnotationTest;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;

@Excel(inFilePath="d:\\studentsModelann.xlsx")
public class Students {
	@Cell(rowNum = 5,columnNum="b")
	private String name;
	@Cell(rowNum = 2,columnNum="c")//or@Cell(columnNum="C")
	private int age;
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "name="+name+":age="+age;
	}
}

package com.easyexcel.readlisttest;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.easyexcel.read.ReadExcel;
import com.easyexcel.read.ReadListExcel;
import com.easyexcel.read.exception.LackCellAnnotation;

public class ReadTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void test() throws InstantiationException, IllegalAccessException, IOException, LackCellAnnotation {
        ReadExcel<Students> re = new ReadListExcel<>();
//		List<Students> list = re.read(null,Students.class);
        List<Students> list = re.read(Students.class);
        System.out.println(list);
    }
}

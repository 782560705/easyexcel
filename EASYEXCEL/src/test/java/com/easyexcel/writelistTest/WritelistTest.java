package com.easyexcel.writelistTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;

import com.easyexcel.util.ExcelUtil;
import com.easyexcel.write.WriteExcel;
import com.easyexcel.write.WriteListExcel;
import com.easyexcel.write.style.CommonCellStyle;
import com.easyexcel.write.style.MyCellStyle;
import com.easyexcel.write.util.WriteListExcelHelp;
import com.easyexcel.writemodelofannotationtest.Grade;

public class WritelistTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testCommon() throws IOException, IllegalArgumentException, IllegalAccessException {
        WriteExcel we = new WriteListExcel();
//		Map<String,Object> param = new HashMap<String, Object>();
//		param.put("outFilePath", "d:\\test.xls");//生成路径必须有，可以传也可以用注解配,如果传以传为主
//		param.put("beginRow", 1);//起始行，可以传也可以用注解配,如果传以传为主，不传取默认值
//		param.put("workbook", new XSSFWorkbook());
        List<Students> list = new ArrayList<Students>();
//		list.add(new Students("张三11111111111111111111111111111111111",12,85));
//		list.add(new Students("李四", 14, 90));
//		list.add(new Students("王五", 14, 90));
        for (int i = 1; i < 500; i++) {
            list.add(new Students("name" + i, i, i));
        }
//		we.write(param, list);
        we.write(list);
    }

    @Test
    public void testCustom() throws IOException, IllegalArgumentException, IllegalAccessException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("test");
        int beginNum = 1;
        MyCellStyle cellStyle = new CommonCellStyle(workbook);
        WriteListExcelHelp weh = new WriteListExcelHelp(cellStyle);
        Map<String, Object> param = new HashMap<String, Object>();
        List<Students> list = new ArrayList<Students>();
//		list.add(new Students("张三",12,85));
//		list.add(new Students("李四", 14, 90));
        for (int i = 1; i < 5000; i++) {
            list.add(new Students("name" + i, i, i));
        }
        weh.generateHeader(sheet, beginNum, Students.class);
        CellStyle commonCellStyle = cellStyle.createCommonCellStyle();
        for (Students s : list) {
            Row row = weh.generateBody(sheet, ++beginNum, s);
            CellRangeAddress cra = new CellRangeAddress(beginNum - 1, beginNum - 1, 1, 2);
            sheet.addMergedRegion(cra);
            row.createCell(2).setCellStyle(commonCellStyle);
        }
        ExcelUtil.workbookToFile(workbook, "d:\\test1.xls");
    }
}
